/* 
  Important for live settings:

  Adjust energy saving settings (no sleep)
  "turn of the wireless network so it won't popup if someone goes by with an open wireless network"
  http://osxdaily.com/2012/04/06/disable-google-chrome-automatic-software-update/
  http://switchtoamac.com/guides/mac-os-x/software-update/disable-automatic-updates-in-software-update.html
  https://forum.processing.org/topic/running-processing-for-an-extend-period-of-time
*/

import processing.video.*;
import processing.serial.*;
import java.awt.event.KeyEvent;

boolean isLinux = System.getProperty("os.name").equals("Linux");

// Device name for the webcam
//   for OSX 64 bits:  "0", "1"
//   for OSX 32 bits:  "USB Video Class Video:0", "USB Video Class Video:1"
//   for Linux:        "/dev/video0",   "/dev/video1"
String sDevice = isLinux ? "/dev/video0" : "Logitech Camera";

// Path to the gphoto2 program. OSX needs full path.
String sGphotoPath = isLinux ? "gphoto2" : "/opt/local/bin/gphoto2";

// Serial port number for Arduino input
String sSerialPortName = isLinux ? "ttyACM" : "tty.usbmodem";

// Aspect ratio for the webcam, thumbnails and preview area
float sAspectRatio = 3.0 / 4.0;

// Webcam refresh rate in frames per second
int sFPS = 30;

// If less than [sTimeBetweenShots] seconds has elapsed since last shot
// the image won't be saved to disk.
int sTimeBetweenShots = 4;

// How many seconds after trigger to wait before capturing webcam image
// IMPORTANT: this value should be lower than sTimeBetweenShots!
float sSaveWebcamDelay = 3; 

// Amount of frames that it takes to fade in a thumbnail
int sFadeDuration = 8;

// Number of columns and rows of thumbnails
int sColumns = 3;
int sRows = 4;

// Load last screenshot on boot, false to start blank
// Note: screenshot is only saved after the grid is full of images.
boolean sLoadScreenshotOnBoot = true;

// Top and bottom margin in pixels
int sMargin = 30;

// Space in pixels between thumbnails
int sPadding = 12;

// Webcam capture resolution in pixels
int sCamWidth = isLinux ? 640 : 960;
int sCamHeight = isLinux ? 480 : 720;

// Application screen size in pixels
int sAppWidth = 1280;
int sAppHeight = 1024;

// Background color, used also as the initial thumbnail fade color
color sBackgroundColor = color(255, 255, 255); // RGB




// Private settings

int sLivePreviewHeight;
int sLivePreviewWidth;
int sLivePreviewX;
float sPreviewScaleX;
float sPreviewScaleY;

// Global variables

Capture webcam;
ThumbFader fader;
Serial arduino;
int timeOfLastCapture = 0;
int saveWebcamAtFrame = -1;
String saveWebcamFilename;

