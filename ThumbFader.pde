/* 
  This class keeps a copy of a thumbnail and a GridRandomizer to know
  where to draw the thumbnail.

  It has a draw() method that is called repeatedly to fade a thumbnail in.
  It does the fading by drawing the image, and a rectangle on top.
  The rectangle becomes more transparent over time.
  When done it sets isFading to false.
*/

class ThumbFader {
  GridRandomizer grid;
  boolean isFading = false;
  int leftBorderX;
  int framesTotal;
  int frameCurrent;
  int currentX;
  int currentY;
  int imgCounter = 0;
  PImage img;
 
  ThumbFader(int w, int h, int x, int frames, int columns, int rows) {
    this.img = createImage(w, h, RGB);
    this.leftBorderX = x;
    this.framesTotal = frames;
    this.grid = new GridRandomizer(columns, rows);
  }
  void draw() {
    image(this.img, this.currentX, this.currentY);

    if(this.frameCurrent == 0) {
      this.isFading = false;
      // a screenshot is saved after the screen is filled with thumbs
      // this screenshot is loaded when the application starts
      if(this.imgCounter > 0 && (this.imgCounter % this.grid.count()) == 0) {
        saveFrame("s" + dateString() + ".jpg");        
      }
      this.imgCounter++;
    } else {
      float opacity = 255 * this.frameCurrent / float(this.framesTotal);
      fill(sBackgroundColor, opacity);
      rect(this.currentX, this.currentY, this.img.width, this.img.height);
      this.frameCurrent--;
    }
  }
  void startFade() {
    this.grid.next();
    this.currentX = this.leftBorderX + this.grid.x()*(this.img.width+sPadding);
    this.currentY = sMargin + this.grid.y()*(this.img.height+sPadding);

    this.frameCurrent = this.framesTotal;
    this.isFading = true;      
    this.prepareThumb();
  }
  void prepareThumb() {
    this.img.copy(g, 
      sLivePreviewX, sMargin, sLivePreviewWidth, sLivePreviewHeight,
      0, 0, this.img.width, this.img.height
    );
  }
}
