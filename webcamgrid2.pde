/* 
  Copyright 2013 by Abe Pazos @hamoid

  02.06.2012 01:22
    First version
  
  11.04.2013 11:17 
    New version for Processing 2.0b8
    Add trigger for external reflex camera
    Separate source code into boot, settings
    Find serial port by name, not by number
    Change app resolution to 1280 x 1024
*/

// Called many times per second
void draw() {
  // While fading a thumbnail it will not pay attention to the
  // webcam or Arduino input
  if (fader.isFading) {
    fader.draw();
  } 
  else {
    if (webcam.available() == true) {
      webcam.read();
      drawLivePreview();
    }
    while (arduino.available() > 0) {
      if (arduino.read() == 1) {
        photoRequested();
      }
    }
  }
  // Save webcam image if enough frames have elapsed
  if(frameCount == saveWebcamAtFrame) {  
    webcam.save(saveWebcamFilename);
    // Tell the thumbnail fader to grab a copy of the webcam preview image
    fader.startFade();
  }
}

// Draws a scaled down version of the webcam feed
void drawLivePreview() {
  loadPixels();
  for (int x=0; x<sLivePreviewWidth; x++) {
    for (int y=0; y<sLivePreviewHeight; y++) {
      int to = sLivePreviewX + x + (y+sMargin) * width;
      int from = int(y*sPreviewScaleX) + int(x*sPreviewScaleY) * webcam.width;
      pixels[to] = webcam.pixels[from];
    }
  }
  updatePixels();
}

// Used to limit the frequency of saving images
boolean enoughTimeHasPassed() {
  return millis() > timeOfLastCapture + sTimeBetweenShots*1000;
}

// A photo was requested either pressing the space bar, or from Arduino
void photoRequested() {
  // If the last photo request is too recent, don't shoot
  if (!enoughTimeHasPassed()) {
    return;
  }

  // If it's fading a thumbnail, don't shoot
  if (fader.isFading) {
    return;
  }

  String date_jpg = dateString() + ".jpg";
  
  // Delayed webcam save image
  // Add 1 in case delay is 0, otherwise it never gets saved
  saveWebcamAtFrame = 1 + frameCount + int((sSaveWebcamDelay*1000)/frameRate);
  saveWebcamFilename = "i" + date_jpg;

  // Capture a photo with the external camera
  String[] params = {
    sGphotoPath, 
    "--capture-image-and-download", 
    "--filename", 
    sketchPath + "/h" + date_jpg
  };
  //println("Calling gphoto2 with these arguments:");
  //println(params);  
  exec(params);
  
  // Remember the current capture time to avoid saving too often
  timeOfLastCapture = millis();
}

String dateString() {
  return nf(year(), 2) + nf(month(), 2) + nf(day(), 2) + "_" + nf(hour(), 2) + nf(minute(), 2) + nf(second(), 2);
}

// Keyboard events
void keyPressed() {
  // The space bar simulates Arduino action
  if (keyCode == KeyEvent.VK_SPACE) {
    photoRequested();
  }
  // The s key saves a screenshot of the application (for debugging)
  if (key == 's' || key == 'S') {
    saveFrame("debug" + dateString() + ".jpg");
  }
  // The ESC key closes the application and tries to shut down the computer
  if (key==ESC) {
    println("shutting down the system");
    open(sketchPath("../shutdown.app"));
  }
}

