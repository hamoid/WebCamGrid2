void setup() {
  size(sAppWidth, sAppHeight);
  background(sBackgroundColor);
  noStroke();

  stopCameraListener();

  if (sLoadScreenshotOnBoot) {
    loadScreenshot();
  }

  // Calculate sizes used for thumbnails and webcam preview area
  int thumbHeight = (sAppHeight - sMargin - sMargin - sPadding * sRows) / sRows;
  int thumbWidth = int(thumbHeight * sAspectRatio);

  sLivePreviewHeight = sAppHeight - sMargin - sMargin - sPadding;
  sLivePreviewWidth = int(sLivePreviewHeight * sAspectRatio);

  int thumbAreaWidth = (thumbWidth + sPadding) * sColumns;
  int thumbX = (sAppWidth - sLivePreviewWidth - thumbAreaWidth) / 2;
  sLivePreviewX = thumbX + thumbAreaWidth;

  sPreviewScaleX = sCamWidth / float(sLivePreviewHeight);
  sPreviewScaleY = sCamHeight / float(sLivePreviewWidth);

  // Create an object that fades thumbnails in on the right place
  fader = new ThumbFader(thumbWidth, thumbHeight, thumbX, sFadeDuration, sColumns, sRows);

  // Optional debug info
  showCameraInfo();
  // showOSinfo();
  showSerialInfo();

  // Start webcam
  startCamera();
  // Start listening to Arduino
  startSerial();
}

void startSerial() {
  String[] ports = Serial.list();
  for(int i=0; i<ports.length; i++) {
    String[] m = match(ports[i], sSerialPortName);
    // If not null, then a match was found
    println (m);
    if (m != null) {
      String portName = Serial.list()[i];
     // arduino = new Serial(this, sSerialPortName, 9600);
      arduino = new Serial(this, portName, 9600);
      return;
    }
  }
  println("Arduino error: could not find port containing '" + sSerialPortName + "'");
  println("Please make sure Arduino is plugged in, and maybe change variable sSerialPortName.");
  exit();
}
void startCamera() {
  webcam = new Capture(this, sCamWidth, sCamHeight, sDevice, sFPS);
  webcam.start();
}

void loadScreenshot() {
  String lastScreenshot = "";
  File[] files = getFileList();
  for (File f : files) {
    String n = f.getName();
    if (n.substring(0, 1).equals("s")) {
      if (n.substring(n.length()-4).equals(".jpg")) {
        lastScreenshot = n.substring(0);
      }
    }
  }
  if (!lastScreenshot.equals("")) {
    PImage ss = loadImage(lastScreenshot);
    image(ss, 0, 0);
  }
}

// Try to kill the OSX PTPCamera service which grabs the reflex camera
// for itself, making gphoto2 not work.
void stopCameraListener() {
  if (isLinux) {
    return;
  }
  try {
    Runtime.getRuntime().exec("killall PTPCamera");
  } 
  catch(IOException e) {
    println("Can not stop PTPCamera");
    println(e);
  }
}

// Return a list of files found in the sketch directory
File[] getFileList() {
  ArrayList<File> filesList = new ArrayList<File>();
  File f = new File(sketchPath("./"));
  return f.listFiles();
}

// Print information about webcams
void showCameraInfo() {
  String[] cameras = Capture.list();

  if (cameras.length == 0) {
    println("There are no cameras available for capture.");
    exit();
  } 
  else {
    println("\nInfo: available webcams:");
    for (int i = 0; i < cameras.length; i++) {
      println(cameras[i]);
    }
  }
}

// Print information about the operating system
void showOSinfo() {
  println("\nInfo: operating system");
  System.getProperties().list(System.out);
}

// Print information about serial ports
void showSerialInfo() {
  println("\nInfo: Available serial ports for Arduino. Use in settings > sSerialPortName.");
  println(Serial.list());
}

