/*
  This class is used for keeping track of a grid of elements
  randomly sorted.

  Calling next() moves to the next location in the grid.

  x() and y() return the current location.

  For a grid of cols=3, x can be numbers between 0 and 2.
  For a grid of rows=4, x can be numbers between 0 and 3.
*/

import java.util.Random;

class GridRandomizer {
  int currentItem = 0;
  int cols = 0;
  int rows = 0;
  int[] items;

  GridRandomizer(int cols, int rows) {
    this.cols = cols;
    this.rows = rows;
    items = new int[cols*rows];
    for(int i=0; i<items.length; i++) {
      items[i] = i;
    }
    shuffle(items);
  }
  void next() {
    currentItem = (currentItem + 1) % items.length;
  }
  int x() {
    return items[currentItem] % cols;
  }
  int y() {
    return items[currentItem] / cols;
  }
  int count() {
    return items.length;
  }
  void shuffle (int[] arr) {
    Random rng = new Random();
    int n = arr.length;
    while (n > 1) {
      int k = rng.nextInt(n); 
      n--;
      int temp = arr[n];
      arr[n] = arr[k];
      arr[k] = temp;
    }
  }

}
